window.onload = function () {
    init();
    var searchbtn = document.getElementById('search-btn');
    var userRef = firebase.database().ref('users_list');
    searchbtn.addEventListener("click",function()
      {
        var searchie = document.getElementById('search-input').value;
        if(searchie != "")
        {
          userRef.once('value').then(function (snapshot)
          {
            snapshot.forEach(function(childshot){
              var data = childshot.val();
              if(data.email === searchie || data.name === searchie)
              {
                document.location.href = "others.html?" + data.email;
              }
            });
          });
        }
      });
    var poker = document.getElementById('poker');
    var wrestling = document.getElementById('wrestling');
    poker.addEventListener("click",pokerClick);
    wrestling.addEventListener("click",wrestlingClick);
};
function pokerClick()
{
  var change = document.getElementById('changing');
  var start = document.getElementById('starter');
  //starter.innerHTML = "<h1 class='page-header'>Catogories</h1>";
  change.innerHTML = "<div class='row'><div class='col-md-2'><button id = 'Cash' type='button' class='btn btn-info'>Cash</button></div>" +
  "<div class='col-md-2'><button id = 'Tourney' type='button' class='btn btn-danger'>Tournament</button></div>" +
  "<div class='col-md-2'><button id = 'previous' type='button' class='btn btn-default'>Previus</button></div>" +
  "<div class='col-md-2'><button id = 'Novice' type='button' class='btn btn-warning'>Novice</button></div>"+
  "<div class='col-md-2'><button id = 'Hand' type='button' class='btn btn-success'>Hand Review</button></div></div>";
  var previous = document.getElementById('previous');
  previous.addEventListener("click",function(e)
  {
    //start.innerHTML = "<h1 class='page-header'>Welcome to XXX forum</h1><p>Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>";
    change.innerHTML = "<div class='row'><div class='col-md-2   text-center'><button id = 'poker' type='button' class='btn btn-warning'>poker</button></div>" +
                       "<div class='col-md-2   text-center'><button id = 'wrestling' type='button' class='btn btn-danger  text-center'>Wrestling</button></div></div>";
    poker.addEventListener("click",pokerClick);
    wrestling.addEventListener("click",wrestlingClick);
  });
  var tourney = document.getElementById('Tourney');
  var cash = document.getElementById('Cash');
  var hand = document.getElementById('Hand');
  var novice = document.getElementById('Novice');
  cash.addEventListener("click",cashClick);
  tourney.addEventListener("click",tourneyClick);
  hand.addEventListener("click",handClick);
  novice.addEventListener("click",noviceClick);
}
function wrestlingClick()
{
  var change = document.getElementById('changing');
  change.innerHTML = "<div class='row'><div class='col-md-2'><button id = 'WWE' type='button' class='btn btn-info'>WWE</button></div>" +
  "<div class='col-md-2'><button id = 'previous' type='button' class='btn btn-default'>Previus</button></div>" +
  "<div class='col-md-2'><button id = 'Independant' type='button' class='btn btn-danger'>Independant Circuit</button></div></div>";
  var previous = document.getElementById('previous');
  previous.addEventListener("click",function(e)
  {
    //start.innerHTML = "<h1 class='page-header'>Welcome to XXX forum</h1><p>Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>";
    change.innerHTML = "<div class='row'><div class='col-md-2   text-center'><button id = 'poker' type='button' class='btn btn-warning'>poker</button></div>" +
                       "<div class='col-md-2   text-center'><button id = 'wrestling' type='button' class='btn btn-danger  text-center'>Wrestling</button></div></div>";
    poker.addEventListener("click",pokerClick);
    wrestling.addEventListener("click",wrestlingClick);

  });
  var wwe = document.getElementById('WWE');
  var independant = document.getElementById('Independant');
  wwe.addEventListener("click",wweClick);
  independant.addEventListener("click",indyClick);
}
function cashClick()
{
  document.location.href = "list.html?Cash";
}
function tourneyClick()
{
  document.location.href = "list.html?Tournament";
}
function handClick()
{
  document.location.href = "list.html?Hand";
}
function noviceClick()
{
  document.location.href = "list.html?Novice"
}
function wweClick()
{
  document.location.href = "list.html?WWE"
}
function indyClick()
{
  document.location.href = "list.html?Independant"
}
