function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    btnLogin.addEventListener('click', function () {
      var email = txtEmail.value;
      var password = txtPassword.value;
      firebase.auth().signInWithEmailAndPassword(email, password).then(function(e)
      {
        create_alert("success",e.message);
        document.location.href = "index.html";
      }).catch(function(e)
      {
        create_alert("error",e.message);
        txtEmail.value = "";
        txtPassword.value = "";
      });
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" and clean input field
    });

    btnGoogle.addEventListener('click', function () {
      btnGoogle.addEventListener('click', e =>
      {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result)
        {
            var token = result.credential.accessToken;
            var user = result.user;
            firebase.database().ref('users_list/' + user.uid).set
            ({
              email:user.email,
              name: user.displayName,
            }).then(function()
            {
              create_alert("success");
              console.log("success");
              document.location.href = "index.html";
            }).catch(function(error)
            {
              console.error("寫入使用者資訊錯誤",error);
            });
            //document.location.href = "index.html";
        }).catch(function (error)
        {
            create_alert("error",error.message);
        });
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
      });
    });
    btnSignUp.addEventListener('click',function()
    {
      var dynamic = document.getElementById('dynamic-button');
      var please = document.getElementById('Please');
      var usernamejump = document.getElementById('dynamic-username')
      dynamic.innerHTML = "<button class='btn btn-lg btn-secondary btn-block' id='btnSignedUp'>Sign up</button>";
      usernamejump.innerHTML = "<label for='inputname' class = 'sr-only'>User name</label><input type='text' id='inputname' class = 'form-control' placeholder='User Name' required autofocus>"
      please.innerHTML = "Register"//"<h1 class='h3 mb-3 font-weight-normal' id = 'Please'>Please sign up</h1>"
      var txtusername = document.getElementById('inputname');
      document.getElementById('btnSignedUp').addEventListener('click', function () {
        /*var email = txtEmail.value;
        var password = txtPassword.value;
        var username = txtusername.value;*/
        //if(usernameExist(txtusername.value) != "false")
        //{
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value,txtPassword.value).then(function()
        {
            var loginUser = firebase.auth().currentUser;
            console.log("登入使用者為",loginUser);
            console.log(txtusername.value);
            loginUser.updateProfile({displayName:txtusername.value}).catch(function(e){console.log(e.message)});
            firebase.database().ref('users_list/' + loginUser.uid).set
            ({
              email:txtEmail.value,
              name: txtusername.value,
            }).then(function()
            {
              create_alert("success");
              console.log("success");
              document.location.href = "index.html";
            }).catch(function(error)
            {
              console.error("寫入使用者資訊錯誤",error);
            });
            /*var data =
            {
              "email" : email,
              "password" : password,
              "username" : username
            };
            xtry(data);*/
        }).catch(function(e)
        {
          create_alert("error",e.message);
          txtEmail.value = "";
          txtPassword.value = "";
          txtusername.value = "";
        });
      //}
      //else {
      //  create_alert("error","the username is Exist");
      //}
    });

        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}
/*function findUserIdByUsername(username) {
  if (!username) {
    return Promise.reject('username is falsy');
  }
  firebase.database().ref('/user_lists').child(username).on('value')
  .then(snapshot => {
    if (!snapshot.exists()) {
      return null; // no user with that username
    }
    return snapshot.val(); // the owner's user ID
  });
}
function usernameExist(username)
{
  var userRef = firebase.database().ref('users_list');
  userRef.once('value').then(function(snapshot)
  {
    snapshot.forEach(function(childshot)
    {
      var data = childshot.val();
      console.log(data);
      console.log(data.name)
      console.log(username);
      if(findUserIdByUsername)
      {

      }
    });
  }).catch(e => console.log(e.message));
}*/
window.onload = function () {
    initApp();
};
