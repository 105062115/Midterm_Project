window.onload = function () {
        var locate = document.location.search.replace("?","");
        var back = locate.substring(locate.indexOf("_")+1);
        var front = locate.slice(0,locate.indexOf("_"));
        var topic = back.substring(back.indexOf("/")+1).replace(/%20/g," ");
        var user = back.slice(0,back.indexOf("/"));
        var postsRef = firebase.database().ref('content_list' + front);
        console.log(postsRef);
        var commentRef;
        init();
        //console.log(front);
        //console.log(user);
        //var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>" + topic + "</h6><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
        var str_before_username = "<div class='page-header'><h3>" + topic + "</h1></div><div style='margin-bottom:2rem;'><a href='others.html?'" + user + "' class = 'small' id = 'user'><strong>"
        var str_after_content = "</p></div></div>"
        //var str_after_content = "</p></div></div>\n";

        // List for store posts html
        var total_post = [];
        // Counter for checking history post update complete
        var first_count = 0;
        // Counter for checking when to update new post
        var second_count = 0;
        var key;
        postsRef.once('value').then(function (snapshot)
        {
          snapshot.forEach(function(childshot){
          var data = childshot.val();

          if(data.user === user && data.topic === topic )
          {
            commentRef = firebase.database().ref('content_list/' + front + '/' + childshot.key + '/comments');
            // List for store posts html
            var total_post_2 = [];
            // Counter for checking history post update complete
            var first_count_2 = 0;
            // Counter for checking when to update new post
            var second_count_2 = 0;
            commentRef.once('value').then(function (snapshot)
            {
              snapshot.forEach(function(childshot){
              var data = childshot.val();
              if(data.commentusername === "undifined")
              {
                total_post_2[total_post_2.length] = "<div class='transbox'><a class = 'small' href='others.html?" + data.user_email + "'><strong>" + data.user_email + "</strong></a><p>" + data.comment + "</p></div>";
              }
              else
              {
                total_post_2[total_post_2.length] = "<div class='transbox'><a class = 'small' href='others.html?" + data.user_email + "'><strong>" + data.commentusername + "</strong></a><p>"
                                                  + data.user_email + "</p><p>" + data.comment + "</p></div>";
              }
              first_count_2 += 1
            });

            document.getElementById('loadcomment').innerHTML = total_post_2.join('');

            commentRef.on('child_added', function (data) {
              second_count_2 += 1;
              if (second_count_2 > first_count_2) {
                var childData = data.val();
                if(childData.commentusername === "undefined")
                {
                  total_post_2[total_post_2.length] = "<div class='transbox'><a class = 'small' href='others.html?" + childData.user_email + "'><strong>" + childData.user_email + "</strong></a><p>" + childData.comment + "</p></div>";
                }
                else
                {
                  total_post_2[total_post_2.length] = "<div class='transbox'><a class = 'small' href='others.html?" + childData.user_email + "'><strong>" + childData.commentusername + "</strong></a><p>"
                                                    + childData.user_email + "</p><p>" + childData.comment + "</p></div>";
                }
                document.getElementById('loadcomment').innerHTML = total_post_2.join('');
              }
            });
          }).catch(function(e)
          {
            create_alert("error",e.message);
          });


            total_post[total_post.length] = str_before_username + data.user +"</strong></a></div><div class = 'border'><div class = 'transbox media'><p>"+ data.article + str_after_content;
          }

          first_count += 1
        });

        document.getElementById('post_list').innerHTML = total_post.join('');

        postsRef.on('child_added', function (data) {
          second_count += 1;
          if (second_count > first_count) {
            var childData = data.val();
            total_post[total_post.length] = str_before_username + data.user + "</strong>" + data.topic + data.article + str_after_content;
            document.getElementById('post_list').innerHTML = total_post.join('');
          }
        });
      }).catch(function(e)
      {
        create_alert("error",e.message);
      });
  }
  function init()
  {
      var user_email = '';
      var searchbtn = document.getElementById('search-btn');
      var userRef = firebase.database().ref('users_list');
      var count = 0;
      searchbtn.addEventListener("click",function()
        {
          var searchie = document.getElementById('search-input').value;
          if(searchie != "")
          {
            userRef.once('value').then(function (snapshot)
            {
              var x = snapshot.numChildren();
              snapshot.forEach(function(childshot){
                var data = childshot.val();
                if(data.email === searchie || data.name === searchie)
                {
                  document.location.href = "others.html?" + data.email;
                  count = 0;
                }
                count++;
                console.log(count);
                if(count == x)
                  alert("This User Name or Email didn't exist.")
              });
            }).then(function()
            {
              count = 0;
            });
          }
          else {
            alert("Please key in who u want to find.")
            return;
          }
        });
      firebase.auth().onAuthStateChanged(function (user)
      {
          var menu = document.getElementById('menu');
          // Check user login
          if (user)
          {
              user_email = user.email;
              if(typeof(user.displayName) === "undifined")
              {
                menu.innerHTML = "<span class='dropdown-item' id='userbtn'>" + user_email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>" +
                                 "<span class='dropdown-item' id = 'post-btn'>Post</span>";
              }
              else {
                menu.innerHTML = "<span class='dropdown-item' id='userbtn'>" + user.displayName + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>" +
                                 "<span class='dropdown-item' id = 'post-btn'>Post</span>";
              }
              var btnLogout = document.getElementById('logout-btn');
              var userbtn = document.getElementById('userbtn');
              var post = document.getElementById('post-btn')
              btnLogout.addEventListener('click', e =>
              {
                  firebase.auth().signOut().then(function(e)
                  {
                    create_alert("success","success");
                    document.location.href = "index.html"
                  }).catch(function(e)
                  {
                    create_alert("error",e.message);
                  });

              });
              userbtn.addEventListener("click",function()
              {
                document.location.href = "user.html";
              })
              post.addEventListener("click",function()
              {
                  document.location.href = "post.html"
              })
              /// TODO 5: Complete logout button event
              ///         1. Add a listener to logout button
              ///         2. Show alert when logout success or error (use "then & catch" syntex)

              var submit = document.getElementById('submit-btn');
              //var commentinput = document.getElementById('comment-input');
              //commentinput.value = "Give some Comment!"
                submit.addEventListener("click",function()
                {
                  var locate = document.location.search.replace("?","");
                  var front = locate.slice(0,locate.indexOf("_"));
                  var back = locate.substring(locate.indexOf("_")+1);
                  var postsRef = firebase.database().ref('content_list' + front);
                  var topic = back.substring(back.indexOf("/")+1).replace(/%20/g," ");
                  var users = back.slice(0,back.indexOf("/"));
                  var commentinput = document.getElementById('comment-input');
                  postsRef.once('value').then(function (snapshot)
                  {
                    snapshot.forEach(function(childshot){
                      var data = childshot.val();
                      if(data.user === users && data.topic === topic)
                      {
                        var commentRef = firebase.database().ref('content_list/' + front + '/' + childshot.key + '/comments');
                        //console.log(user.email);
                        commentRef.push({
                               commentusername : user.displayName,
                               comment : "<pre>" + commentinput.value + "</pre>",
                               user_email : user.email,
                         }).then(function()
                         {
                             console.log("success");
                             commentinput.value = "";
                         }).catch(function()
                         {
                             console.log("failed");
                         });
                      }
                      else {
                        //console.log(data.user);
                        //console.log(data.topic);
                        commentinput.innerHTML = "";
                        console.log("failed");
                      }
                    });
                  });
                });

          }
          else
          {
              var commentinput = document.getElementById('texie');
              commentinput.innerHTML =   "<textarea rows='5' id='comment-input' placeholder='Please Login to comment!''>"
              menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
              //document.getElementById('post_list').innerHTML = "";
              var submit = document.getElementById('submit-btn');
              submit.addEventListener("click",function()
              {
                alert("Please login to comment");
              });
          }
        });
  }
  function create_alert(type, message) {
      var alertarea = document.getElementById('custom-alert');
      if (type == "success") {
          str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
          alertarea.innerHTML = str_html;
      } else if (type == "error") {
          str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
          alertarea.innerHTML = str_html;
      }
  }
