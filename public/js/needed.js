function init()
{
    var user_email = '';
    var searchbtn = document.getElementById('search-btn');
    var userRef = firebase.database().ref('users_list');
    var count = 0;
    searchbtn.addEventListener("click",function()
      {
        var searchie = document.getElementById('search-input').value;
        if(searchie != "")
        {
          userRef.once('value').then(function (snapshot)
          {
            var x = snapshot.numChildren();
            snapshot.forEach(function(childshot){
              var data = childshot.val();
              if(data.email === searchie || data.name === searchie)
              {
                document.location.href = "others.html?" + data.email;
                count = 0;
              }
              count++;
              console.log(count);
              if(count == x)
                alert("This User Name or Email didn't exist.")
            });
          }).then(function()
          {
            count = 0;
          });
        }
        else {
          alert("Please key in who u want to find.")
          return;
        }
      });
    firebase.auth().onAuthStateChanged(function (user)
    {
        var menu = document.getElementById('menu');
        // Check user login
        if (user)
        {
            user_email = user.email;
            if(typeof(user.displayName) === "undifined")
            {
              menu.innerHTML = "<span class='dropdown-item' id='userbtn'>" + user_email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>" +
                               "<span class='dropdown-item' id = 'post-btn'>Post</span>";
            }
            else {
              menu.innerHTML = "<span class='dropdown-item' id='userbtn'>" + user.displayName + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>" +
                               "<span class='dropdown-item' id = 'post-btn'>Post</span>";
            }
            var btnLogout = document.getElementById('logout-btn');
            var userbtn = document.getElementById('userbtn');
            var post = document.getElementById('post-btn')
            btnLogout.addEventListener('click', e =>
            {
                firebase.auth().signOut().then(function(e)
                {
                  create_alert("success","sucess");
                  document.location.href = "index.html"
                }).catch(function(e)
                {
                  create_alert("error",e.message);
                });

            });
            userbtn.addEventListener("click",function()
            {
              document.location.href = "user.html";
            })
            post.addEventListener("click",function()
            {
                document.location.href = "post.html"
            })
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
        }
        else
        {
            // It won't show any post if not login.
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            //document.getElementById('post_list').innerHTML = "";
        }
      });
}
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}
function xtry(data)
{
  firebase.database().ref().child('user_list').push(data).then(function(e)
  {
    firebase.database().ref("user_list").update(e);
  }).catch(function()
  {
    document.location.href = "index.html";
  });
}
