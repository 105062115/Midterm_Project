# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name] Jimbo's Forum
* Key functions (add/delete)
    1. user page
    2. post page
    3. post list page
    4. leave comment under any post
* Other functions (add/delete)
    1. search user name
    2. randomly display post preview
    3. Click username at post/comment can link to his user page
    4. you can upload photo at your user page

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
  作品網址 : [https://105062115.gitlab.io/Midterm_Project/index.html]
  報告網址 : [https://gitlab.com/105062115/Midterm_Project/blob/master/README.md]
    簡介 :
      任何使用者都可以觀看論壇裡所有類別的文章內容，但必須要登入才能夠發文、發布評論。
      首頁下方有提供隨機類別的最新文章preview，並提供文章連結。若該類別並無文章，下方會提供試登入狀況提供登入/po文連結
      在文章網頁中po文者與評論者的名字/電子郵件都可點入觀看此user資料。
      在文章下方評論時如果未登入，點選submit會跳出alert提醒必須登入。
      除此之外，右上角的search欄位也可搜尋user帳戶名稱。
      在頁面上方的bar中的dropdown 若在非登入狀態可以login，在登入狀態點選自己的名字可以進入自己的userpage。
      Logout與post功能也提供在此dropdown列。
      點選Forum則可以進入文章標題分類，點選所要的分類後便可以看到此分類的文章目錄。
    實作方法 :
      透過存取"content_list"與"users_liSt"，在database上我會存取user資料與文章、留言資料。
      content部分
        在網頁間透過query string向所去的網頁索要某個帳號或某個文章內容的資訊。
        以foreach尋找資料庫中符合querystring提供的發文者email以及符合的文章標題並將該文章post在頁面上。
        在存取文章時我會在文章前後加上pre的tag，讓文章保持輸入者輸入的型式。
      user 部分
        sign in page本身是用lab6所提供的function。
        不管從google端登入或是自己註冊帳號，都會把基本user資料存進database。
        user page的部分則是把database的資料拿下來顯示，若此用戶沒有該項資料將會顯示空白。
        上傳大頭貼的功能則是會把照片存到storage，並把downloadURL存到user的資料裡。
      其他 部分
        首頁的隨機貼文
          利用Math.random()並用round()取到亂數，在利用switch判定哪個變數要顯示哪個類別。
        search功能
          利用foreach搜尋符合的user email資料或是user name資料，並且連至另外寫的user page(others.html)
          並用querystring紀錄所查的帳號。(others.html與user.html分別寫了2個html，為了保障security)。
## Security Report (Optional)
  為了保護user，自己的userpage與別人看到的userpage我寫成兩個網頁(user.html/others.html)。
  並且在others.html無法修改用戶資料
  如此一來，登入的user必定無法從user.html登入至別人可修改的userpage，若進入user.html一定會進到自己的page
  而others.html就算修改query string也只是改看別人的資料，既看不到私密資料，也沒有edit button可用，無法修改。
